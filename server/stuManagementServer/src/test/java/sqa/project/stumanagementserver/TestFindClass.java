package sqa.project.stumanagementserver;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
public class TestFindClass {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testFindClass() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/findClass/{name}_{year}", "giai tich", 2022))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.[0].year").value(2022))
        .andExpect(MockMvcResultMatchers.jsonPath("$.[0].semester").value(1))
        .andExpect(MockMvcResultMatchers.jsonPath("$.[0].subject.id").value("BAS0033"))
        .andExpect(MockMvcResultMatchers.jsonPath("$.[0].subject.name").value("Giai tich"))
        .andExpect(MockMvcResultMatchers.jsonPath("$.[0].lecturer_id").value("CNTT001"));
    }
}
