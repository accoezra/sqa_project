package sqa.project.stumanagementserver;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import lombok.Getter;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import sqa.project.stumanagementserver.entity.Student;
import sqa.project.stumanagementserver.entity.StudentScore;
import sqa.project.stumanagementserver.repository.StudentScoreRepository;
import sqa.project.stumanagementserver.service.StudentScoreService;

import java.util.List;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = StumanagementserverApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("dev")
public class StudentScoreEditTest {
    @Autowired
    private MockMvc mvc;
    @Autowired
    private StudentScoreRepository repository;
    @Autowired
    private StudentScoreService service;
    @Rule
    final public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("restdocs");
    @Autowired
    private WebApplicationContext context;

    @Before
    public void setUp() {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.context)
                .apply(documentationConfiguration(this.restDocumentation))
                .build();
    }
    @Test
    public void test_edit_score_badRequest() throws Exception {
        StudentScore testScore = repository.findByClassroomId(1).get(0);
        ObjectMapper objectMapper = new ObjectMapper();

        mvc.perform(post("/student-score/save/1")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(
                                List.of(new TestObject1(testScore.getStudent().getId(),
                                        testScore.getDiligence(),
                                        testScore.getHomework(),
                                        testScore.getTest(),
                                        testScore.getLab(),
                                        testScore.getExam())))
                        ))
                .andExpect(status().is4xxClientError()).andDo(document("score-edit-badRequest"));
    }

    @Test
    public void test_edit_score_success() throws Exception {
        StudentScore testScore = repository.findByClassroomId(1).get(0);
        ObjectMapper objectMapper = new ObjectMapper();

        mvc.perform(post("/student-score/save/1")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(
                                List.of(new TestObject2(testScore.getStudent().getId(),
                                        String.valueOf(testScore.getDiligence()),
                                        String.valueOf(testScore.getTest()),
                                        String.valueOf(testScore.getHomework()),
                                        String.valueOf(testScore.getLab()),
                                        String.valueOf(testScore.getExam()))))
                        ))
                .andExpect(status().isOk()).andDo(document("score-edit-success"));
    }

    @Test
    public void test_edit_score_dataNotFound() throws Exception {
        StudentScore testScore = repository.findByClassroomId(1).get(0);
        testScore.setStudent(new Student("B19DCCN111", "~", 19, "~", "~"));
        ObjectMapper objectMapper = new ObjectMapper();

        mvc.perform(post("/student-score/save/1")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(
                                List.of(new TestObject2(testScore.getStudent().getId(),
                                        String.valueOf(testScore.getDiligence()),
                                        String.valueOf(testScore.getTest()),
                                        String.valueOf(testScore.getHomework()),
                                        String.valueOf(testScore.getLab()),
                                        String.valueOf(testScore.getExam()))))
                        ))
                .andExpect(status().is5xxServerError()).andDo(document("score-edit-dataNotFound"));
    }

    @Test
    public void test_edit_score_service() throws Exception {
        List<StudentScore> scores = service.getStudentScoresByClassroomId(3);

        assert(scores.size() == 0);
    }

    @Getter
    class TestObject1 {
        private final String id;
        private final double diligence, homework, test, lab, exam;

        public TestObject1(String id, double diligence, double homework, double test, double lab, double exam) {
            this.id = id;
            this.diligence = diligence;
            this.homework = homework;
            this.test = test;
            this.lab = lab;
            this.exam = exam;
        }
    }

    @Getter
    class TestObject2 {
        private final String student_id;
        private final String CC, KT, BT, TH, Thi;

        public TestObject2(String student_id, String CC, String KT, String BT, String TH, String thi) {
            this.student_id = student_id;
            this.CC = CC;
            this.KT = KT;
            this.BT = BT;
            this.TH = TH;
            this.Thi = thi;
        }
    }

}
