package sqa.project.stumanagementserver;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import sqa.project.stumanagementserver.entity.User;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = StumanagementserverApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class LoginTest {
    @Autowired
    private MockMvc mvc;

    @Rule
    final public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("restdocs");
    @Autowired
    private WebApplicationContext context;

    @Before
    public void setUp() {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.context)
                .apply(documentationConfiguration(this.restDocumentation))
                .build();
    }

    @Test
    public void test_login_success() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        mvc.perform(post("/signin").contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(new User("HIEUcnt001", "abcdef001"))))
                .andExpect(status().isOk())
                .andDo(document("login-success"));
    }

    @Test
    public void test_login_failure() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        mvc.perform(post("/signin").contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(new User("HIEUcnf001", "abcdef001"))))
                .andExpect(status().is4xxClientError())
                .andDo(document("login-failure"));
    }
}
