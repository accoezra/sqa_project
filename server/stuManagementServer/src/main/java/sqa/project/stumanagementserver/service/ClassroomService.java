package sqa.project.stumanagementserver.service;

import sqa.project.stumanagementserver.entity.Classroom;
import sqa.project.stumanagementserver.exceptions.DataNotFoundException;

public interface ClassroomService {
    Classroom getClassroomById(int id) throws DataNotFoundException;
}
