package sqa.project.stumanagementserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import sqa.project.stumanagementserver.entity.*;
import sqa.project.stumanagementserver.repository.*;

@Configuration
public class LoadDatabase {
    private final LecturerRepository lecturerRepository;
    private final UserRepository userRepository;
    private final StudentScoreRepository studentScoreRepository;
    private final ClassroomRepository classroomRepository;
    private final StudentRepository studentRepository;
    private final ClassTimeRepository classTimeRepository;
    private final SubjectRepository subjectRepository;

    @Autowired
    public LoadDatabase(LecturerRepository lecturerRepository, UserRepository userRepository, StudentRepository studentRepository,
                            ClassroomRepository classroomRepository, StudentScoreRepository studentScoreRepository,
                            ClassTimeRepository classTimeRepository, SubjectRepository subjectRepository) {
        this.lecturerRepository = lecturerRepository;
        this.userRepository = userRepository;
        this.studentRepository = studentRepository;
        this.studentScoreRepository = studentScoreRepository;
        this.classroomRepository = classroomRepository;
        this.classTimeRepository = classTimeRepository;
        this.subjectRepository = subjectRepository;
    }

    @Bean
    CommandLineRunner initDb() {
        return args -> {
            //teacher and user
            Lecturer lecturer1 = new Lecturer("CNTT001", "Le Trong Hieu",
                    "190 Nguyen Trai, Gia Lam, Ha Noi", "lthieu22@gmail.com", "0777111333", 37);
            lecturerRepository.save(lecturer1);
            userRepository.save(new User("HIEUcnt001", "abcdef001", lecturer1));

            //subject and classroom
            Subject mathSubject = new Subject("BAS0033", "Giai tich", 3);
            Subject basicSubject = new Subject("BAS0035", "Triet hoc", 2);
            Classroom newClass = new Classroom(2022,1, 1, mathSubject, lecturer1);

            //classtime
            ClassTime newClassTime = new ClassTime(12, "A1", ClassTime.ClassType.NORMAL, ClassTime.DAYS.MONDAY, 4, newClass);

            //students
            Student s1 = new Student("B19DCCN232", "Tran Trung Huy", 18, "hloa@yahoo.com", "random address");
            Student s2 = new Student("B19DCCN253", "Trinh Thi Nhi", 18, "nhil@yahoo.com", "random address 2");
            newClass.addStudent(s1);
            newClass.addStudent(s2);

            //studentRepository.save(s1);
            //studentRepository.save(s2);

            subjectRepository.save(mathSubject);
            subjectRepository.save(basicSubject);

            classroomRepository.save(newClass);
            classTimeRepository.save(newClassTime);
        };
    }
}
