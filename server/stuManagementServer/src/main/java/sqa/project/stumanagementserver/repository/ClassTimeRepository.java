package sqa.project.stumanagementserver.repository;

import org.springframework.data.repository.CrudRepository;
import sqa.project.stumanagementserver.entity.ClassTime;

public interface ClassTimeRepository extends CrudRepository<ClassTime, Integer> {
}
