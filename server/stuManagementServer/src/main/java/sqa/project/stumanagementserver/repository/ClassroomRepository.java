package sqa.project.stumanagementserver.repository;

import org.springframework.data.repository.CrudRepository;
import sqa.project.stumanagementserver.entity.Classroom;

public interface ClassroomRepository extends CrudRepository<Classroom, Integer> {
}
