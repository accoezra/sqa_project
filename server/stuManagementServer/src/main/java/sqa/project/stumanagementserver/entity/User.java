package sqa.project.stumanagementserver.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.Getter;
import org.hibernate.validator.constraints.Length;

@Entity
@Getter
public class User {
    @Pattern(regexp = "^[0-9a-zA-Z]+$", message = "Username doesn't follow the format")
    @Length(min = 4, max = 20, message = "Username length incorrect")
    @NotBlank
    private @Id String username;

    @Pattern(regexp = "([A-Za-z]+[0-9]|[0-9]+[A-Za-z])[A-Za-z0-9]*", message = "Password doesn't follow the format")
    @Length(min = 8, max = 20, message = "Password length incorrect")
    @NotBlank
    private String password;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "lecturer", referencedColumnName = "id")
    private Lecturer lecturer;

    public User(String username, String password, Lecturer lecturer) {
        this.username = username;
        this.password = password;
        this.lecturer = lecturer;
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
        this.lecturer = null;
    }


    public User() {}
}
