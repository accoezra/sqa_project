package sqa.project.stumanagementserver.service;

import org.springframework.security.core.userdetails.UserDetails;
import sqa.project.stumanagementserver.entity.User;

public interface UserService {
    UserDetails signin(String username, String password);
}
