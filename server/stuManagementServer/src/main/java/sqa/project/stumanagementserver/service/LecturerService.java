package sqa.project.stumanagementserver.service;

import sqa.project.stumanagementserver.entity.Lecturer;

import java.util.Optional;

public interface LecturerService {
    Optional<Lecturer> getLecturerByUser(String username);
}
