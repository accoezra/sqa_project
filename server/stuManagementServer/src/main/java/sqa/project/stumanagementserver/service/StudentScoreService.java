package sqa.project.stumanagementserver.service;

import sqa.project.stumanagementserver.entity.Classroom;
import sqa.project.stumanagementserver.entity.StudentScore;

import java.util.List;
import java.util.Map;

public interface StudentScoreService {

    List<StudentScore> getStudentScores();

    List<StudentScore> getStudentScoresByClassroomId(int id);

    boolean findAndSaveScore(Map<String, String> score, int clas_id);
}
