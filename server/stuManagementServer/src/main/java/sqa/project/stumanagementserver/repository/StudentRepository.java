package sqa.project.stumanagementserver.repository;

import org.springframework.data.repository.CrudRepository;
import sqa.project.stumanagementserver.entity.Student;

public interface StudentRepository extends CrudRepository<Student, String> {

}
