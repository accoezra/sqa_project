package sqa.project.stumanagementserver.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sqa.project.stumanagementserver.entity.StudentScore;
import sqa.project.stumanagementserver.repository.StudentScoreRepository;
import sqa.project.stumanagementserver.service.StudentScoreService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
class StudentScoreServiceImpl implements StudentScoreService {
    @Autowired
    private final StudentScoreRepository ssr; //too lazy

    public StudentScoreServiceImpl(StudentScoreRepository studentScoreRepository) {
        ssr = studentScoreRepository;
    }

    public void saveStudentScore(List<StudentScore> scores) {
        for (StudentScore studentScore : scores) {
            ssr.save(studentScore);
        }
    }

    @Override
    public List<StudentScore> getStudentScores() {
        List<StudentScore> results = new ArrayList<StudentScore>();
        ssr.findAll().forEach(results::add);

        return results;
    }

    @Override
    public List<StudentScore> getStudentScoresByClassroomId(int id) {
        List<StudentScore> results = new ArrayList<StudentScore>();
        ssr.findByClassroomId(id).forEach(results::add);

        return results;
    }

    @Override
    public boolean findAndSaveScore(Map<String, String> scoreData, int clas_id) {

        Optional<StudentScore> oScore = ssr.findByClassroomIdAndStudentId(clas_id, scoreData.get("student_id"));

        if (oScore.isEmpty())
            return false;

        StudentScore score = oScore.get();
        score.setDiligence(Double.parseDouble(scoreData.get("cc")));
        score.setHomework(Double.parseDouble(scoreData.get("bt")));
        score.setTest(Double.parseDouble(scoreData.get("kt")));
        score.setLab(Double.parseDouble(scoreData.get("th")));
        score.setExam(Double.parseDouble(scoreData.get("thi")));
        ssr.save(score);

        return true;
    }
}
