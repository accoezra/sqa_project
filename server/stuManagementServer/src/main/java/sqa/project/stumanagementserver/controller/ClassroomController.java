package sqa.project.stumanagementserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sqa.project.stumanagementserver.entity.Classroom;
import sqa.project.stumanagementserver.exceptions.DataNotFoundException;
import sqa.project.stumanagementserver.service.ClassroomService;

@RestController
@RequestMapping(value = "/classroom/", produces = MediaType.APPLICATION_JSON_VALUE)
public class ClassroomController {
    ClassroomService classroomService;

    @Autowired
    ClassroomController(ClassroomService classroomService) {
        this.classroomService = classroomService;
    }

    @GetMapping("/{classroom_id}")
    public ResponseEntity<Classroom> getClassroomById(@PathVariable("classroom_id") int id) {
        Classroom classroom;
        try {
            classroom = classroomService.getClassroomById(id);
        } catch (DataNotFoundException ex) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(classroom);
    }
}
