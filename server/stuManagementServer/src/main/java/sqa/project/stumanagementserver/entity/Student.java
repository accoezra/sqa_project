package sqa.project.stumanagementserver.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.annotation.Nullable;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Entity
@Getter @Setter @NoArgsConstructor
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Student {
    private @Id String id;

    @NotBlank(message = "Name of student can't be blank")
    private String name;

    @NotNull
    @Min(value = 10)
    private int age;

    @Email private String email;

    @Nullable private String address;

    @ManyToMany(mappedBy = "students")
    @JsonIgnore
    private Set<Classroom> classrooms;

    @OneToMany(mappedBy = "student")
    @JsonIgnore
    private Set<StudentScore> scores;

    public Student(String id, String name, int age, String email, String address) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.email = email;
        this.address = address;
    }
}
