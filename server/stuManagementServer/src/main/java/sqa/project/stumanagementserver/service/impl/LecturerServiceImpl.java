package sqa.project.stumanagementserver.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sqa.project.stumanagementserver.entity.Lecturer;
import sqa.project.stumanagementserver.entity.User;
import sqa.project.stumanagementserver.repository.LecturerRepository;
import sqa.project.stumanagementserver.repository.UserRepository;
import sqa.project.stumanagementserver.service.LecturerService;

import java.util.Optional;

@Service
public class LecturerServiceImpl implements LecturerService {
    private final LecturerRepository lecturerRepository;
    private final UserRepository userRepository;

    @Autowired
    public LecturerServiceImpl(LecturerRepository lecturerRepository, UserRepository userRepository) {
        this.lecturerRepository = lecturerRepository;
        this.userRepository = userRepository;
    }
    public Optional<Lecturer> getLecturerByUser(String username) {
        Optional<User> user = userRepository.findById(username);

        if (user.isEmpty())
            return Optional.empty();

        return Optional.of(user.get().getLecturer());
    }
}
