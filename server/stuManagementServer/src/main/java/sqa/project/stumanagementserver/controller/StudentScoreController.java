package sqa.project.stumanagementserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sqa.project.stumanagementserver.entity.StudentScore;
import sqa.project.stumanagementserver.service.StudentScoreService;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/student-score/", produces = MediaType.APPLICATION_JSON_VALUE)
public class StudentScoreController {
    @Autowired
    private final StudentScoreService ssv;

    StudentScoreController(StudentScoreService studentScoreService) {
        this.ssv = studentScoreService;
    }

    @GetMapping("/all")
    public List<StudentScore> getAllScores() {
        List<StudentScore> scores = ssv.getStudentScores();

        //assert here ...
        return scores;
    }

    @GetMapping("/{classroom_id}")
    public List<StudentScore> getScoresByClassroom(@PathVariable("classroom_id") int clas_id) {
        List<StudentScore> scores = ssv.getStudentScoresByClassroomId(clas_id);
        return scores;
    }

    @PostMapping(value = "/save/{classroom_id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> saveScoresInClassroom(@PathVariable("classroom_id") int clas_id,
                                                @RequestBody List<Map<String, String>> data) {
        //System.out.println(data);

        for (Map<String, String> student : data) {
            if (!student.containsKey("student_id"))
                return new ResponseEntity<>("Missing parameter(s) in request", HttpStatusCode.valueOf(400));
            if (!ssv.findAndSaveScore(student, clas_id))
                return new ResponseEntity<>("Troubling finding and saving student records", HttpStatusCode.valueOf(500));
        }

        return ResponseEntity.ok().build();
    }
}
