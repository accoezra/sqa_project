package sqa.project.stumanagementserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import sqa.project.stumanagementserver.repository.UserRepository;
import sqa.project.stumanagementserver.service.CustomUserDetailsService;

@Configuration
@EnableWebSecurity
@Profile("test")
public class WebSecurityConfig {

    private MyAuthenticationEntryPoint authenticationEntryPoint;
    private UserRepository userRepository;

    @Autowired
    WebSecurityConfig(MyAuthenticationEntryPoint myAuthenticationEntryPoint, UserRepository userRepository) {
        this.authenticationEntryPoint = myAuthenticationEntryPoint;
        this.userRepository = userRepository;
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public CustomUserDetailsService customUserDetailsService() {
        return new CustomUserDetailsService(userRepository);
    }
    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeHttpRequests()
                .requestMatchers("/signin").anonymous()
                .requestMatchers("/h2-console/**").permitAll()
                .anyRequest().hasRole("ROLE_STAFF")
                .and().httpBasic().authenticationEntryPoint(authenticationEntryPoint);

        return http.build();
    }

    /* public TokenAuthenticationFilter restAuthenticationFilter() throws Exception {
        final TokenAuthenticationFilter filter = new TokenAuthenticationFilter(PROTECTED_URLS);
        filter.setAuthenticationManager(authenticationManager());
        filter.setAuthenticationSuccessHandler(successHandler());
        return filter;
    } */
}

