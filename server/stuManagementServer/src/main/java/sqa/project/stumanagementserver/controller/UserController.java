package sqa.project.stumanagementserver.controller;

import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sqa.project.stumanagementserver.entity.User;
import sqa.project.stumanagementserver.service.UserService;

@RestController
public class UserController {
    @Autowired
    UserService userService;

    @RequestMapping(value = "/signin", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> signin(@RequestBody @Valid User user, HttpSession httpSession) {
        UserDetails userDetails = userService.signin(user.getUsername(), user.getPassword());

        if (userDetails == null) {
            return ResponseEntity.notFound().build();
        }

        httpSession.setAttribute("user", userDetails);
        return ResponseEntity.ok(user);
    }
}
