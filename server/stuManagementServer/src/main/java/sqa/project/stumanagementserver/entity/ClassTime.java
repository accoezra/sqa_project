package sqa.project.stumanagementserver.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter @Setter @NoArgsConstructor
public class ClassTime {
    public ClassTime(int roomNumber, String buildingCode, ClassType type, DAYS day, int shift, Classroom classroom) {
        this.roomNumber = roomNumber;
        this.buildingCode = buildingCode;
        this.type = type;
        this.day = day;
        this.shift = shift;
        this.classroom = classroom;
    }

    public enum DAYS {
        MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY
    }

    public enum ClassType {
        NORMAL, LAB, OTHER
    }

    private @Id @GeneratedValue long id;

    @NotNull(message = "Room number can't be null")
    @Min(value = 1, message = "Room number must be larger than 0")
    private int roomNumber;

    @NotBlank(message = "Building code can't be blank!")
    @Pattern(regexp = "^A[1-3]")
    private String buildingCode;

    @Enumerated(EnumType.STRING)
    private ClassType type;

    @Enumerated(EnumType.STRING)
    private DAYS day;

    @NotNull
    @Min(value = 1)
    @Max(value = 6)
    private int shift;

    @ManyToOne
    @JoinColumn(name = "classroom_id", referencedColumnName = "id" , nullable = false)
    private Classroom classroom;
}
