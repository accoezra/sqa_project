package sqa.project.stumanagementserver.service;

import sqa.project.stumanagementserver.entity.Student;

import java.util.List;

public interface StudentService {
    List<Student> getStudents();

    void saveStudent(Student student);
}
