package sqa.project.stumanagementserver.exceptions;

public class DataNotFoundException extends Exception {
    public DataNotFoundException(String errMsg) {
        super(errMsg);
    }
}
