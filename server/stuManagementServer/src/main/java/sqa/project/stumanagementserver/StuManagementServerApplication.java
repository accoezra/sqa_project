package sqa.project.stumanagementserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StumanagementserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(StumanagementserverApplication.class, args);
	}

}
