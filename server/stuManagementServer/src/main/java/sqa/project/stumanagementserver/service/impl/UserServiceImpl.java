package sqa.project.stumanagementserver.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import sqa.project.stumanagementserver.repository.UserRepository;
import sqa.project.stumanagementserver.service.CustomUserDetailsService;
import sqa.project.stumanagementserver.service.UserService;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    CustomUserDetailsService customUserDetailsService;
    @Autowired
    UserRepository userRepository;
    public UserDetails signin(String username, String password) {
        UserDetails user = customUserDetailsService.loadUserByUsername(username);

        if (user.getUsername().equals("guest"))
            return null;

        if (!password.equals(user.getPassword()))
            return null;

        return user;
    }
}
