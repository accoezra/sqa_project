package sqa.project.stumanagementserver.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Value;

import java.util.Set;

@Entity
@Getter @Setter @NoArgsConstructor
public class Subject {
    private @Id String id;

    @Length(max = 50)
    private String name;

    @Min(value = 1)
    @Max(value = 10)
    private int credits;

    @NotNull @Value("0.1")
    private double diligence;
    @NotNull @Value("0.1")
    private double homework;
    @NotNull @Value("0.1")
    private double test;
    @NotNull @Value("0.0")
    private double lab;
    @NotNull @Value("0.7")
    private double exam;

    @OneToMany(mappedBy = "subject")
    @JsonIgnore
    private Set<Classroom> classrooms;

    public Subject(String id, String name, int credits, double diligence, double homework, double test, double lab, double exam) {
        this.id = id;
        this.name = name;
        this.credits = credits;
        this.diligence = diligence;
        this.homework = homework;
        this.test = test;
        this.lab = lab;
        this.exam = exam;
    }

    public Subject(String id, String name, int credits) {
        this.id = id;
        this.name = name;
        this.credits = credits;
        diligence = homework = test = 0.1;
        lab = 0;
        exam = 0.7;
    }
}
