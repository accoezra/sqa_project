package sqa.project.stumanagementserver.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sqa.project.stumanagementserver.entity.Classroom;
import sqa.project.stumanagementserver.exceptions.DataNotFoundException;
import sqa.project.stumanagementserver.repository.ClassroomRepository;
import sqa.project.stumanagementserver.service.ClassroomService;

import java.util.Optional;

@Service
public class ClassroomServiceImpl implements ClassroomService {
    @Autowired
    private final ClassroomRepository classroomRepository;

    public ClassroomServiceImpl(ClassroomRepository classroomRepository) {
        this.classroomRepository = classroomRepository;
    }
    public Classroom getClassroomById(int id) throws DataNotFoundException {
        Optional<Classroom> myclass = classroomRepository.findById(id);

        if (myclass.isEmpty()) {
            throw new DataNotFoundException("Invalid classroom!");
        }

        return myclass.get();
    }
}
