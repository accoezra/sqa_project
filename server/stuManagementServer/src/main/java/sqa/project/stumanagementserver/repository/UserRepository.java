package sqa.project.stumanagementserver.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sqa.project.stumanagementserver.entity.User;

public interface UserRepository extends CrudRepository<User, String> {
    User findByUsername(String username);
}
