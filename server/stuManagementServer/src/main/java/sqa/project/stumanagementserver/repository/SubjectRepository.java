package sqa.project.stumanagementserver.repository;

import org.springframework.data.repository.CrudRepository;
import sqa.project.stumanagementserver.entity.Subject;

public interface SubjectRepository extends CrudRepository<Subject, String> {
}
