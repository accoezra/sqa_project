package sqa.project.stumanagementserver.repository;

import org.springframework.data.repository.CrudRepository;
import sqa.project.stumanagementserver.entity.Lecturer;

public interface LecturerRepository extends CrudRepository<Lecturer, String> {
}
