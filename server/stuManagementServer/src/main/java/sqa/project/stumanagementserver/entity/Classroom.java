package sqa.project.stumanagementserver.entity;

import com.fasterxml.jackson.annotation.*;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Entity @NoArgsConstructor
public class Classroom {
    private @Id @GeneratedValue long id;

    @NotNull
    @Min(value = 1990)
    @Getter @Setter
    private int year;

    @NotNull
    @Getter @Setter
    @Min(value = 1)
    @Max(value = 3)
    private int semester;

    @NotNull @Getter @Setter
    @Min(value = 1) @Max(value = 30)
    private int groupNumber;

    @ManyToOne @Getter @Setter
    @JoinColumn(name = "subject_id", referencedColumnName = "id" , nullable = false)
    private Subject subject;

    @ManyToOne @Getter @Setter
    @JoinColumn(name = "lecturer_id", referencedColumnName = "id",nullable = false)
    @JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
    @JsonIdentityReference(alwaysAsId=true)
    @JsonProperty("lecturer_id")
    private Lecturer lecturer;

    @ManyToMany @Getter @Setter
    @JoinTable(name = "classroom_students",
            joinColumns = @JoinColumn(name = "classroom_id"),
            inverseJoinColumns = @JoinColumn(name = "student_id"))
    @JsonIgnore
    private Set<Student> students;

    @Getter
    @OneToMany(mappedBy = "classroom", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<ClassTime> classTime;

    @Getter
    @OneToMany(mappedBy = "classroom", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<StudentScore> classScores;

    public Classroom(int year, int semester, int groupNumber, Subject subject, Lecturer lecturer) {
        this.year = year;
        this.semester = semester;
        this.groupNumber = groupNumber;
        this.subject = subject;
        this.lecturer = lecturer;
        this.students = new HashSet<Student>();
        this.classScores = new HashSet<StudentScore>();
    }

    public void addStudent(Student student) {
        StudentScore newStudentScore = new StudentScore(student, this);
        this.students.add(student);
        this.classScores.add(newStudentScore);
    }
}
