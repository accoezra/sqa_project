package sqa.project.stumanagementserver.repository;

import org.springframework.data.repository.CrudRepository;
import sqa.project.stumanagementserver.entity.Classroom;
import sqa.project.stumanagementserver.entity.StudentScore;

import java.util.List;
import java.util.Optional;

public interface StudentScoreRepository extends CrudRepository<StudentScore, Integer> {
    List<StudentScore> findByClassroomId(int id);

    Optional<StudentScore> findByClassroomIdAndStudentId(int clas_id, String stu_id);
}
