package sqa.project.stumanagementserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sqa.project.stumanagementserver.entity.Lecturer;
import sqa.project.stumanagementserver.service.LecturerService;

import java.util.Optional;

@RestController
@RequestMapping(value = "/lecturer", produces = MediaType.APPLICATION_JSON_VALUE)
public class LecturerController {
    private final LecturerService lecturerService;

    @Autowired
    public LecturerController(LecturerService lecturerService) {
        this.lecturerService = lecturerService;
    }
    @GetMapping("/get")
    public Lecturer getLecturerByUser(@PathVariable String username) {
        Optional<Lecturer> lecturer = lecturerService.getLecturerByUser(username);

        if (lecturer.isEmpty())
            return null;

        return lecturer.get();
    }
}
