package sqa.project.stumanagementserver.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter @Setter @NoArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class StudentScore {
    @Id @GeneratedValue
    private int id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "student_id", referencedColumnName = "id", nullable = false)
    private Student student;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "classroom_id", referencedColumnName = "id", nullable = false)
    private Classroom classroom;

    @DecimalMin(value = "0.0") @DecimalMax(value = "10.0")
    @NotNull
    private double diligence;

    @DecimalMin(value = "0.0") @DecimalMax(value = "10.0")
    @NotNull
    private double homework;

    @DecimalMin(value = "0.0") @DecimalMax(value = "10.0")
    @NotNull
    private double test;

    @DecimalMin(value = "0.0") @DecimalMax(value = "10.0")
    @NotNull
    private double lab;

    @DecimalMin(value = "0.0") @DecimalMax(value = "10.0")
    @NotNull
    private double exam;

    public StudentScore(Student student, Classroom classroom, double diligence,
                        double homework, double test, double lab, double exam) {
        this.student = student;
        this.classroom = classroom;
        this.diligence = diligence;
        this.homework = homework;
        this.test = test;
        this.lab = lab;
        this.exam = exam;
    }

    public StudentScore(Student student, Classroom classroom) {
        this.student = student;
        this.classroom = classroom;
        diligence = homework = test = lab = exam = 0.0;
    }
}
