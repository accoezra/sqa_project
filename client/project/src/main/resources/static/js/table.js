document.querySelector("#savescores").addEventListener("click", e => {
    saveScores();
});

function saveScores() {
     //Loop over th and crate column Header array
    let table = document.querySelector("table");
    let tlength = table.rows.length;
    let tableContent = []

    for (let i = 1; i < tlength; i++) {
        var row = table.rows.item(i).cells;
        let score = {}

        score["student_id"] = row.item(1).innerHTML;
        score["cc"] = row.item(3).innerHTML;
        score["bt"] = row.item(4).innerHTML;
        score["kt"] = row.item(5).innerHTML;
        score["th"] = row.item(6).innerHTML;
        score["thi"] = row.item(7).innerHTML;
        tableContent.push(score);
    }
    let clas_id = location.href.substring(location.href.lastIndexOf('/') + 1);

    const tableJson = JSON.stringify(tableContent);
    console.log(tableJson);

    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
           document.location.href = "/scores/" + clas_id;
        }
    };
    xmlhttp.open("POST", "http://localhost:8081/scores/edit/" + clas_id, true);
    xmlhttp.setRequestHeader("Content-type", "application/json");
    xmlhttp.send(tableJson);
}

function replaceArrayValue(arr, oldVal, newVal) {
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] === oldVal) {
      arr[i] = newVal;
    }
  }
  return arr;
}