package sqa.project.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class Classroom {
    private int id;
    private int year;
    private int semester;
    private int groupNumber;
    private double avg;
    private String lecturer_id;
    private Subject subject;
}
