package sqa.project.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class Score {
    private int id;
    private double diligence, homework, test, lab, exam;
    private double finalScore;
    private Classroom classroom;
    private Student student;
    public void calculateFinal() {
        finalScore = diligence * classroom.getSubject().getDiligence()
                + homework * classroom.getSubject().getHomework()
                + test * classroom.getSubject().getTest()
                + lab * classroom.getSubject().getLab()
                + exam * classroom.getSubject().getExam();
    }
}
