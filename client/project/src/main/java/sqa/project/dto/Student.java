package sqa.project.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class Student {
    private String id;
    private String name;
    private String address;
    private String email;
    int age;
}
