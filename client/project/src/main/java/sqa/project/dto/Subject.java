package sqa.project.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class Subject {
    private String id;
    private String name;
    private int credits;
    private double diligence, homework, test, lab, exam;
}
