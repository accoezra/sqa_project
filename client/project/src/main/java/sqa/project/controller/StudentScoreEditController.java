package sqa.project.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import sqa.project.dto.Score;
import sqa.project.service.StudentScoreService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/scores/edit")
public class StudentScoreEditController {
    @Autowired
    private final StudentScoreService studentScoreService;

    StudentScoreEditController(StudentScoreService studentScoreService) {
        this.studentScoreService = studentScoreService;
    }

    @GetMapping("/{clas_id}")
    public String scoresEditPage(@PathVariable("clas_id") int clas_id, Model model){
        String content = studentScoreService.getScoresByClass(clas_id).block();
        JSONArray scores = new JSONArray(content);
        //System.out.println("Debug " + scores.get(0).toString());

        List<Score> scoreDtoList = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            for (int i = 0; i < scores.length(); ++i) {
                JSONObject jo = scores.getJSONObject(i);
                Score score = objectMapper.readValue(jo.toString(), Score.class);
                score.calculateFinal();
                scoreDtoList.add(score);
            }
        } catch (JsonProcessingException ex) {
            System.out.println(ex.getMessage());
            return "redirect:/404";
        }

        model.addAttribute("classroom", scoreDtoList.get(0).getClassroom());
        model.addAttribute("scores", scoreDtoList);
        return "score_writer.html";
    }

    @PostMapping("/{clas_id}")
    public ResponseEntity<String> saveScores(@RequestBody List<Map<String, String>> json, @PathVariable("clas_id") int clas_id) {
        json.forEach(jo -> {
            for (Map.Entry<String, String> me : jo.entrySet()) {
                System.out.println(me.getKey() + " " + me.getValue());
            }
        });

        if (studentScoreService.saveScoresByClass(json, clas_id))
            return ResponseEntity.ok().build();
        return ResponseEntity.badRequest().build();
    }
}
