package sqa.project.controller;

import ch.qos.logback.core.model.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
    @GetMapping("/404")
    public String page404() {
        return "p404.html";
    }

    @GetMapping("/")
    public String indexPage() {
        return "redirect:/home";
    }

    @GetMapping("/home")
    public String homePage() {
        return "home.html";
    }

    @GetMapping("/searchClass")
    public String searchResultPage(Model model) {
        //todo: add logic here
        return "home.html";
    }
}
