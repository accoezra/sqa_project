package sqa.project.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import sqa.project.dto.Classroom;
import sqa.project.dto.Score;
import sqa.project.service.StudentScoreService;
import sqa.project.utils.Constant;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/scores")
@Controller
public class StudentScoreController {
    @Autowired
    private final StudentScoreService studentScoreService;

    StudentScoreController(StudentScoreService studentScoreService) {
        this.studentScoreService = studentScoreService;
    }
    @GetMapping("/{clas_id}")
    public String scoresEditPage(@PathVariable("clas_id") int clas_id, Model model){
        String content = studentScoreService.getScoresByClass(clas_id).block();
        JSONArray scores = new JSONArray(content);
        //System.out.println("Debug " + scores.get(0).toString());

        List<Score> scoreDtoList = new ArrayList<>();
        Classroom classroom;
        ObjectMapper objectMapper = new ObjectMapper();
        double classAvg = 0;
        int countNonSatisfyStudent = 0;
        try {

            for (int i = 0; i < scores.length(); ++i) {
                JSONObject jo = scores.getJSONObject(i);
                Score score = objectMapper.readValue(jo.toString(), Score.class);
                score.calculateFinal();
                double finaScore = score.getFinalScore();
                classAvg += finaScore;
                if(finaScore < Constant.MIN_FINAL_SCORE_TO_SATISFY_EXAM_ENTRANCE){
                    ++countNonSatisfyStudent;
                }
                scoreDtoList.add(score);
            }
            classroom = scoreDtoList.get(0).getClassroom();
            classroom.setAvg(classAvg/scores.length());
        } catch (JsonProcessingException ex) {
            System.out.println(ex.getMessage());
            return "redirect:/404";
        }

        model.addAttribute("classroom", classroom);
        model.addAttribute("scores", scoreDtoList);
        model.addAttribute("num_non_satisfy_student", countNonSatisfyStudent);
        double ratio = (double)countNonSatisfyStudent/scores.length()*100;
        model.addAttribute("ratio_non_satisfy_student", (int)ratio);


        return "scores.html";
    }
}
