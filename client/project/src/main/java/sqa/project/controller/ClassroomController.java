package sqa.project.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import sqa.project.model.Classroom;


@Controller
@RequestMapping("/findClass")
public class ClassroomController {
    
    @GetMapping
    public String findClass(Model model) {
        model.addAttribute("subject");
        model.addAttribute("year");
        return "home";
    }

    @GetMapping("/view")
    public String viewFindClass(Model model, @RequestParam String subject, @RequestParam String year) {
        subject = subject.trim();
        year = year.trim();

        model.addAttribute("subject", subject);
        model.addAttribute("year", year);

        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/classroom/findClass/"+subject+"_"+year;
        ResponseEntity<Classroom[]> response = restTemplate.getForEntity(url, Classroom[].class);
        List<Classroom> classrooms = Arrays.asList(response.getBody());
        for (Classroom classroom : classrooms) {
            System.out.println(classroom.getSubject().getName());
        }

        model.addAttribute("classrooms", classrooms);

        return "home";
    }
}
