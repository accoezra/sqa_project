package sqa.project.service;

import org.json.JSONArray;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;

@Service
public class StudentScoreService {
    private WebClient webClient;

    StudentScoreService() {
        webClient = WebClient.builder().baseUrl("http://localhost:8080")
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .build();
    }

    public Mono<String> getScoresByClass(int clas_id) {
        return this.webClient.get().uri("/student-score/{clas_id}", clas_id)
                .retrieve().bodyToMono(String.class);
    }

    public boolean saveScoresByClass(List<Map<String, String>> scores, int clas_id) {
        HttpStatusCode statusCode = this.webClient.post().uri("/student-score/save/{clas_id}", clas_id)
                .bodyValue(new JSONArray(scores).toString())
                .exchangeToMono(response -> Mono.just(response.statusCode()))
                .block();

        assert statusCode != null;
        System.out.println(statusCode.value());
        if (statusCode.is2xxSuccessful())
            return true;

        return false;
    }
}
