package sqa.project.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class Classroom {
    private int year;
    private int semester;
    private Subject subject;
    private String lecturer_id;
    private int groupNumber;
    private int id;

    public Classroom(int year, int semester, Subject subject, String lecturer_id){
        this.year = year;
        this.semester = semester;
        this.lecturer_id = lecturer_id;
        this.subject = subject;
    }

    public Classroom() {

    }
    public class Subject {
        private String id, name;
        private int credits;
        private double diligence, homework, test, lab, exam;

        public Subject(String id, String name, int credits, double diligence, double homework, double test, double lab, double exam) {
            this.id = id;
            this.name = name;
            this.credits = credits;
            this.diligence = diligence;
            this.homework = homework;
            this.test = test;
            this.lab = lab;
            this.exam = exam;
        }

        public Subject() {
            
        }

        public int getCredits() {
            return credits;
        }
        
        public void setCredits(int credits) {
            this.credits = credits;
        }

        public double getDiligence() {
            return diligence;
        }
        public void setDiligence(double diligence) {
            this.diligence = diligence;
        }
        public double getExam() {
            return exam;
        }
        public void setExam(double exam) {
            this.exam = exam;
        }
        public double getHomework() {
            return homework;
        }
        public void setHomework(double homework) {
            this.homework = homework;
        }
        public String getId() {
            return id;
        }
        public void setId(String id) {
            this.id = id;
        }
        public double getLab() {
            return lab;
        }
        public void setLab(double lab) {
            this.lab = lab;
        }
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
        public double getTest() {
            return test;
        }
        public void setTest(double test) {
            this.test = test;
        }
    }
}
