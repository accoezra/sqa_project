package sqa.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StumanagementclientApplication {

	public static void main(String[] args) {
		SpringApplication.run(StumanagementclientApplication.class, args);
	}

}
